-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: pizza_planeta
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `cliente_id` varchar(36) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `comuna` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`cliente_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES ('fe78f9f6-1a98-4651-b3e2-47317fb14636','226327130','San Isidro 91','Santiago','rsilvaldeg@gmail.com');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locales`
--

DROP TABLE IF EXISTS `locales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locales` (
  `local_id` varchar(36) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `codigo_sucursal` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipo_local` varchar(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`local_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locales`
--

LOCK TABLES `locales` WRITE;
/*!40000 ALTER TABLE `locales` DISABLE KEYS */;
INSERT INTO `locales` VALUES ('2e2c1dba-5dfa-11e7-b835-478adfbf2060','Santa Lucia','SL','S'),('c436bf2e-6695-11e7-9bd0-c11731add41d','Casa Matrix','CM','M');
/*!40000 ALTER TABLE `locales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `producto_id` varchar(36) COLLATE utf8_spanish_ci NOT NULL,
  `codigo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `precio_unitario` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`producto_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES ('07a9e851-5fb5-419f-874a-610bf5c328f9','NAPO1','Napolitana','Tomate jamon aceitunas',5000),('4527e582-27e0-43b0-840c-d6e1b6d7af01','lucas','Tester','Tester, obvio',1),('47d48b7e-7d3e-42f4-b53b-bfcde4eac914','leche','pizzatresleches','Con mucha leche',5000),('4bfa68d4-7198-4c9d-99a6-c925dd53575b','aa001','asd','da',9998888777),('4c94d2b0-73cd-4394-b678-6442926cc16e','16501','65106','165160',1000),('4f08ca50-b6d3-430f-9d65-65b22cc551fe','12A1','Pizza motoquera','extra queso',10990),('593d271f-fa06-45a5-b0c3-26056297afac','VEG01','Vegana','Pizza con queso de tofu y vegetales',7000),('5942d979-7bc5-4cf0-8a25-b689e9457e34','00001','pizzaconpichi','tiene pichi',10),('6416e571-7eba-45a9-9c8a-138247565bcb','XXX','E HEH ','XXX',6969),('82284507-5a4f-4308-b6a3-65b2fe16493f','001','Aersh Pizza','Pizza con Aersh',1),('8357b8ec-4e16-43bc-9c94-743e61fe1d12','KK00','Pizza a la piedra base','Piza solo con salsa de tomates a la piedra',5000),('87a6e01f-41a1-44cb-967c-f02a84441f42','00002','pizzaconcaca','tiene caca',10000),('9d4b093e-e849-4052-b7da-8c714b0cd21d','proc01','Producto1','Producto 1 prueba',10000),('a2018731-20a4-49ea-a633-96451bf2f866','0808','Tester2','Tester2, obvio',10),('a874edc8-ea7c-451d-a5e0-de5684628758','0003','pizza1','descripcion pizza',10999),('b3bd115f-060d-446b-b7a2-53a773f39961','990','Espaola','Pizza con chorizillo, aceituna y jamn',7899),('cd7786cf-6a3e-40e5-9bac-7e772a3a372f','0009','Pizza DXQ','Pizza doble queso mozarella',10000),('d8ed5a0b-2d39-4c19-8fa5-9ddecb23a49d','fDF5','Hawaiana','Con mucho queso',1200),('dffab31a-0462-48c0-b780-b21b49d17ed8','ALEMANA','Pizza alemana','Con chorizillo y chucrut',7888),('eb1d9f85-912e-4439-8c36-1852bce2424a','KK','Con KK','HKQK',789),('f261756b-6e91-4fee-983c-c23f9258c73c','8787','Sacarias Flores Del Campo','Pizza solo masa',120000);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendedores`
--

DROP TABLE IF EXISTS `vendedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendedores` (
  `vendedor_id` varchar(36) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `codigo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`vendedor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendedores`
--

LOCK TABLES `vendedores` WRITE;
/*!40000 ALTER TABLE `vendedores` DISABLE KEYS */;
INSERT INTO `vendedores` VALUES ('1baffab2-5dfa-11e7-b835-478adfbf2060','JUanito Perez','01');
/*!40000 ALTER TABLE `vendedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventas`
--

DROP TABLE IF EXISTS `ventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventas` (
  `venta_id` varchar(36) COLLATE utf8_spanish_ci NOT NULL,
  `monto_total` bigint(20) DEFAULT NULL,
  `fecha_creacion` datetime DEFAULT NULL,
  `cliente_id` varchar(36) COLLATE utf8_spanish_ci DEFAULT NULL,
  `local_id` varchar(36) COLLATE utf8_spanish_ci DEFAULT NULL,
  `vendedor_id` varchar(36) COLLATE utf8_spanish_ci DEFAULT NULL,
  `folio` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`venta_id`),
  KEY `fk_ventas_locales1_idx` (`local_id`),
  KEY `fk_ventas_clientes1_idx` (`cliente_id`),
  KEY `fk_ventas_vendedores1_idx` (`vendedor_id`),
  CONSTRAINT `fk_ventas_clientes1` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`cliente_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ventas_locales1` FOREIGN KEY (`local_id`) REFERENCES `locales` (`local_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ventas_vendedores1` FOREIGN KEY (`vendedor_id`) REFERENCES `vendedores` (`vendedor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventas`
--

LOCK TABLES `ventas` WRITE;
/*!40000 ALTER TABLE `ventas` DISABLE KEYS */;
INSERT INTO `ventas` VALUES ('180fc095-7a83-476a-b7a8-aafff6d6bdd7',1000,'2017-07-03 23:18:40','fe78f9f6-1a98-4651-b3e2-47317fb14636','2e2c1dba-5dfa-11e7-b835-478adfbf2060','1baffab2-5dfa-11e7-b835-478adfbf2060',4),('2459204e-5073-4afd-b555-af6f78550599',1000,'2017-07-03 23:14:14','fe78f9f6-1a98-4651-b3e2-47317fb14636','2e2c1dba-5dfa-11e7-b835-478adfbf2060','1baffab2-5dfa-11e7-b835-478adfbf2060',2),('60fd9cfa-a489-4454-b950-79fd1f3dcf5a',1000,'2017-07-03 23:20:19','fe78f9f6-1a98-4651-b3e2-47317fb14636','2e2c1dba-5dfa-11e7-b835-478adfbf2060','1baffab2-5dfa-11e7-b835-478adfbf2060',6),('7baf8aee-03f7-4567-97c7-69cd7c44426b',1000,'2017-07-03 23:10:39','fe78f9f6-1a98-4651-b3e2-47317fb14636','2e2c1dba-5dfa-11e7-b835-478adfbf2060','1baffab2-5dfa-11e7-b835-478adfbf2060',1),('904aaeca-2e19-4b22-9361-f4e9b7e3e168',1000,'2017-07-03 23:21:17','fe78f9f6-1a98-4651-b3e2-47317fb14636','2e2c1dba-5dfa-11e7-b835-478adfbf2060','1baffab2-5dfa-11e7-b835-478adfbf2060',7),('96c2655f-e386-48fc-bd45-4efa4ed6ac1e',1000,'2017-07-03 23:18:50','fe78f9f6-1a98-4651-b3e2-47317fb14636','2e2c1dba-5dfa-11e7-b835-478adfbf2060','1baffab2-5dfa-11e7-b835-478adfbf2060',5),('d956a1e9-95c1-489b-8dee-c621f8db1cb3',1000,'2017-07-03 23:25:17','fe78f9f6-1a98-4651-b3e2-47317fb14636','2e2c1dba-5dfa-11e7-b835-478adfbf2060','1baffab2-5dfa-11e7-b835-478adfbf2060',8),('f87a2253-acf9-4bbb-9998-9152af4b2643',1000,'2017-07-03 23:17:01','fe78f9f6-1a98-4651-b3e2-47317fb14636','2e2c1dba-5dfa-11e7-b835-478adfbf2060','1baffab2-5dfa-11e7-b835-478adfbf2060',3);
/*!40000 ALTER TABLE `ventas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventas_detalles`
--

DROP TABLE IF EXISTS `ventas_detalles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventas_detalles` (
  `venta_detalle_id` varchar(36) COLLATE utf8_spanish_ci NOT NULL,
  `venta_id` varchar(36) COLLATE utf8_spanish_ci DEFAULT NULL,
  `producto_id` varchar(36) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `detalle_total` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`venta_detalle_id`),
  KEY `fk_ventas_detalles_ventas_idx` (`venta_id`),
  KEY `fk_ventas_detalles_productos1_idx` (`producto_id`),
  CONSTRAINT `fk_ventas_detalles_productos1` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`producto_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ventas_detalles_ventas` FOREIGN KEY (`venta_id`) REFERENCES `ventas` (`venta_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventas_detalles`
--

LOCK TABLES `ventas_detalles` WRITE;
/*!40000 ALTER TABLE `ventas_detalles` DISABLE KEYS */;
INSERT INTO `ventas_detalles` VALUES ('2cf09b2b-d2e8-4ebe-a0ce-71501e19775b','7baf8aee-03f7-4567-97c7-69cd7c44426b','593d271f-fa06-45a5-b0c3-26056297afac',1,1000),('4b5f9d27-0e2c-40fd-a396-a8434b547445','904aaeca-2e19-4b22-9361-f4e9b7e3e168','07a9e851-5fb5-419f-874a-610bf5c328f9',1,1000),('64278b8c-0356-409e-83e1-4cd29d5827ac','2459204e-5073-4afd-b555-af6f78550599','07a9e851-5fb5-419f-874a-610bf5c328f9',1,1000),('67ecdc00-330d-4e61-b4d1-9fa4e3da727c','180fc095-7a83-476a-b7a8-aafff6d6bdd7','07a9e851-5fb5-419f-874a-610bf5c328f9',1,1000),('8f4b08ab-16dc-47f4-965f-a6256af14558','96c2655f-e386-48fc-bd45-4efa4ed6ac1e','07a9e851-5fb5-419f-874a-610bf5c328f9',1,1000),('a3d41762-0443-4174-a5fb-1aca7e08fd87','60fd9cfa-a489-4454-b950-79fd1f3dcf5a','07a9e851-5fb5-419f-874a-610bf5c328f9',1,1000),('d56faf84-bfb2-47cb-a5cf-d56c61f23435','d956a1e9-95c1-489b-8dee-c621f8db1cb3','07a9e851-5fb5-419f-874a-610bf5c328f9',1,1000),('db8b62ae-ea88-47c8-a845-9fcbc9f7aa0a','f87a2253-acf9-4bbb-9998-9152af4b2643','07a9e851-5fb5-419f-874a-610bf5c328f9',1,1000);
/*!40000 ALTER TABLE `ventas_detalles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-11 21:20:10
