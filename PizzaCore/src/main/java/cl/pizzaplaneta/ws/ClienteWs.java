/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.pizzaplaneta.ws;

import cl.pizzaplaneta.dao.ClienteDao;
import cl.pizzaplaneta.dto.ClienteDto;
import cl.pizzaplaneta.modelos.Cliente;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import org.hibernate.HibernateException;

/**
 *
 * @author ricardo
 */
/**
 * @WebService(serviceName = "Venta") public class ClienteWs {
 *
 *
 * @WebResult(name="pedido")
 * @WebMethod(operationName = "registrarPedido") public String registrarPedido(
 * @WebParam(name = "name") String txt ) { return "Hello " + txt + " !"; }
 */
/**
 * String id; String telefono; String direccion; String comuna; String email;
 */
@WebService(serviceName = "Cliente")
public class ClienteWs {

    /**
     * This is a sample web service operation
     */
    @WebResult(name = "Cliente")
    @WebMethod(operationName = "registrarCliente")
    public ClienteDto registrarCliente(
            @WebParam(name = "telefono") String telefono,
            @WebParam(name = "direccion") String direccion,
            @WebParam(name = "comuna") String comuna,
            @WebParam(name = "email") String email
    ) {
        ClienteDto clienteDto = new ClienteDto();

        if (telefono == null || telefono.equals("")) {
            clienteDto.setCode(2);
            clienteDto.setMessage("debe introducir telefono");
            return clienteDto;
        }
        ClienteDao cDao = new ClienteDao();
        cDao.iniciarTransaccion(); //inicia la transaccion en la base de datos
        HashMap fields = new HashMap<>();
        fields.put("telefono", telefono);
        Cliente existe = cDao.getUniqueByFields(fields);
        if (existe != null) {
            clienteDto.setCode(2);
            clienteDto.setMessage("cliente ya existe con este telefono: " + telefono);
            return clienteDto;
        }

        cDao.iniciarTransaccion(); //inicia la transaccion en la base de datos
        Cliente cliente = new cl.pizzaplaneta.modelos.Cliente();
        cliente.setId(UUID.randomUUID().toString());
        cliente.setTelefono(telefono);
        cliente.setDireccion(direccion);
        cliente.setComuna(comuna);
        cliente.setEmail(email);

        try {

            cDao.insert(cliente); //intento insertar el campo en la BD
            cDao.commit(); //si no da error, hago commit

            clienteDto.setId(cliente.getId());
            clienteDto.setTelefono(cliente.getTelefono());
            clienteDto.setMessage("Graba correctamente!");
            clienteDto.setDireccion(cliente.getDireccion());
            clienteDto.setComuna(cliente.getComuna());
            clienteDto.setEmail(cliente.getEmail());
            return clienteDto;

        } catch (HibernateException exception) {
            exception.printStackTrace();
            cDao.rollback(); //si hay algun error, hago rollback y no se aplica ningun cambio a la BD

            clienteDto.setCode(200);
            clienteDto.setMessage(exception.getLocalizedMessage());
            return clienteDto;

        }

    }

    @WebResult(name = "cliente")
    @WebMethod(operationName = "consultarCliente")
    public ClienteDto consultarCliente(
            @WebParam(name = "telefono") String telefono
    ) {
        ClienteDto clienteDto = new ClienteDto();

        if (telefono == null || telefono.equals("")) {
            clienteDto.setCode(2);
            clienteDto.setMessage("debe introducir telefono");
            return clienteDto;
        }
        ClienteDao cDao = new ClienteDao();
        cDao.iniciarTransaccion(); //inicia la transaccion en la base de datos
        HashMap fields = new HashMap<>();
        fields.put("telefono", telefono);
        Cliente cliente = cDao.getUniqueByFields(fields);
        if (cliente != null) {
            clienteDto.setCode(200);
            clienteDto.setMessage("OK");
            clienteDto.setTelefono(cliente.getTelefono());
            clienteDto.setDireccion(cliente.getDireccion());
            clienteDto.setComuna(cliente.getComuna());
            clienteDto.setEmail(cliente.getEmail());
            return clienteDto;

        } else {
            clienteDto.setCode(2);
            clienteDto.setMessage("Cliente no fue encontrado");
            return clienteDto;

        }

    }
    /**
     * TODO: consultarCliente registrarCliente registrarProducto consultarPizza
     * registrarVenta
     */
}
