/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.pizzaplaneta.ws;

import cl.pizzaplaneta.dao.ProductoDao;
import cl.pizzaplaneta.dto.ProductoDto;
import cl.pizzaplaneta.modelos.Producto;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import org.hibernate.HibernateException;

/**
 *
 * @author ricardo
 */
@WebService(serviceName = "Producto")
public class ProductoWs {

    /**
     * This is a sample web service operation
     */
    @WebResult(name = "producto")
    @WebMethod(operationName = "registrarProducto")
    public ProductoDto registrarProducto(
            @WebParam(name = "nombre") String nombre,
            @WebParam(name = "descripcion") String descripcion,
            @WebParam(name = "precio") Long precio,
            @WebParam(name = "codigo") String codigo
    ) {
        ProductoDto productoDto = new ProductoDto();

       boolean isLong = precio instanceof Long;
        
        if (precio == null || !isLong) {
            productoDto.setCode(201);
            productoDto.setMessage("Precio no es un numero valido");
            return productoDto;
        }

        if (nombre == null || nombre.equals("")) {
            productoDto.setCode(201);
            productoDto.setMessage("debe introducir nombre");
            return productoDto;
        }
        if (codigo == null || codigo.equals("")) {
            productoDto.setCode(201);
            productoDto.setMessage("debe introducir codigo");
            return productoDto;
        }
        ProductoDao pDao = new ProductoDao();
        pDao.iniciarTransaccion(); //inicia la transaccion en la base de datos
        HashMap fields = new HashMap<>();
        fields.put("codigo", codigo);
        Producto existe = pDao.getUniqueByFields(fields);
        if (existe != null) {
            productoDto.setCode(201);
            productoDto.setMessage("Producto ya existe con este codigo: " + codigo);
            return productoDto;
        }

        pDao.iniciarTransaccion(); //inicia la transaccion en la base de datos
        Producto producto = new cl.pizzaplaneta.modelos.Producto();
        producto.setId(UUID.randomUUID().toString());
        producto.setNombre(nombre);
        producto.setPrecioUnitario(precio);
        producto.setDescripcion(descripcion);
        producto.setCodigo(codigo);

        try {

            pDao.insert(producto); //intento insertar el campo en la BD
            pDao.commit(); //si no da error, hago commit

            productoDto.setCode(200);
            productoDto.setId(producto.getId());
            productoDto.setMessage("Graba correctamente!");
            productoDto.setDescripcion(producto.getDescripcion());
            productoDto.setNombre(producto.getNombre());
            productoDto.setPrecioUnitario(producto.getPrecioUnitario());
            return productoDto;

        } catch (HibernateException exception) {
            exception.printStackTrace();
            pDao.rollback(); //si hay algun error, hago rollback y no se aplica ningun cambio a la BD

            productoDto.setCode(201);
            productoDto.setMessage(exception.getLocalizedMessage());
            return productoDto;

        }

    }

    @WebResult(name = "producto")
    @WebMethod(operationName = "consultarPizza")
    public ProductoDto consultarPizza(
            @WebParam(name = "codigo") String codigo
    ) {
        ProductoDto productoDto = new ProductoDto();

        if (codigo == null || codigo.equals("")) {
            productoDto.setCode(2);
            productoDto.setMessage("debe introducir codigo");
            return productoDto;
        }
        ProductoDao pDao = new ProductoDao();
        pDao.iniciarTransaccion(); //inicia la transaccion en la base de datos
        HashMap fields = new HashMap<>();
        fields.put("codigo", codigo);
        Producto producto = pDao.getUniqueByFields(fields);
        if (producto != null) {
            productoDto.setCode(200);
            productoDto.setMessage("OK");
            productoDto.setCodigoProducto(producto.getCodigo());
            productoDto.setDescripcion(producto.getDescripcion());
            productoDto.setPrecioUnitario(producto.getPrecioUnitario());
            productoDto.setNombre(producto.getNombre());
            return productoDto;
        } else {
            productoDto.setCode(2);
            productoDto.setMessage("Producto no fue encontrado");
            return productoDto;

        }

    }
    /**
     * TODO: consultarCliente registrarCliente registrarVenta
     */
}
