/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.pizzaplaneta.ws;

import cl.pizzaplaneta.dao.ClienteDao;
import cl.pizzaplaneta.dao.LocalDao;
import cl.pizzaplaneta.dao.ProductoDao;
import cl.pizzaplaneta.dao.VendedorDao;
import cl.pizzaplaneta.dao.VentaDao;
import cl.pizzaplaneta.dao.VentaDetalleDao;
import cl.pizzaplaneta.dto.DetalleDto;
import cl.pizzaplaneta.dto.VentaDto;
import cl.pizzaplaneta.modelos.Cliente;
import cl.pizzaplaneta.modelos.Local;
import cl.pizzaplaneta.modelos.Producto;
import cl.pizzaplaneta.modelos.Vendedor;
import cl.pizzaplaneta.modelos.Venta;
import cl.pizzaplaneta.modelos.VentaDetalle;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Michel M. <michel@febos.cl>
 */
@WebService(serviceName = "Pizzeria")
public class Pizzeria {

    /**
     * Web service operation
     *
     */
    @WebMethod(operationName = "realizarVenta")
    public VentaDto realizarVenta(
            @WebParam(name = "telefonoCliente") String telefonoCliente,
            @WebParam(name = "direccionCliente") String direccionCliente,
            @WebParam(name = "detalle") List<DetalleDto> detalles,
            @WebParam(name = "codigoVendedor") String codigoVendedor,
            @WebParam(name = "codigoLocal") String codigoLocal
    ) {

        VentaDto response = new VentaDto();
        VentaDao ventaDao = new VentaDao();
        ventaDao.iniciarTransaccion();
        try {
            String ventaId = UUID.randomUUID().toString();

            Long montoTotal = 0L;

            // VentaDao ventaDao = new VentaDao();
            List<VentaDetalle> lstVentas = new ArrayList<>();
            ProductoDao productoDao = new ProductoDao();
            productoDao.iniciarTransaccion();
            for (DetalleDto detalle : detalles) {

                HashMap fieldsProducto = new HashMap<>();
                fieldsProducto.put("codigo", detalle.getCodigoPizza());
                System.out.println("CONSULTA PRODUCTO: " + detalle.getCodigoPizza());
                Producto p = productoDao.getUniqueByFields(fieldsProducto);
                if (p != null) {
                    VentaDetalle ventaDetalle = new VentaDetalle();

                    ventaDetalle.setId(UUID.randomUUID().toString());
                    ventaDetalle.setProductoId(p.getId());
                    ventaDetalle.setVentaId(ventaId);
                    ventaDetalle.setCantidad(detalle.getCantidad());
                    ventaDetalle.setDetalleTotal(p.getPrecioUnitario() * detalle.getCantidad());
                    montoTotal += p.getPrecioUnitario();

                    lstVentas.add(ventaDetalle);

                } else {
                    productoDao.commit();
                    System.out.println("PRODUCTO NULL");
                    response.setCode(201);
                    response.setMessage("Producto con codigo: " + detalle.getCodigoPizza() + " No existe");
                    return response;
                }

            }
            productoDao.commit();
            Cliente c = null;
            ClienteDao cliDao = new ClienteDao();
            cliDao.iniciarTransaccion();
            HashMap fieldsCliente = new HashMap<>();

            fieldsCliente.put("telefono", telefonoCliente.trim());
            c = cliDao.getUniqueByFields(fieldsCliente);
            if (null == c) {
                c = new Cliente();
                c.setId(UUID.randomUUID().toString());
                c.setTelefono(telefonoCliente);
                c.setDireccion(direccionCliente);
                try {
                    cliDao.insert(c);
                    cliDao.commit();
                } catch (Exception e) {
                    cliDao.rollback();
                }
            } else {
                c.setDireccion(direccionCliente);
                try {
                    cliDao.update(c);
                    cliDao.commit();
                } catch (Exception e) {
                    cliDao.rollback();
                }

            }
            Local local = null;
            LocalDao locaDao = new LocalDao();
            locaDao.iniciarTransaccion();
            HashMap fieldsLocal = new HashMap<>();
            fieldsLocal.put("codigo_sucursal", codigoLocal);
            local = locaDao.getUniqueByFields(fieldsLocal);
            locaDao.commit();
            Vendedor vendedor = null;
            VendedorDao vendeDao = new VendedorDao();
            vendeDao.iniciarTransaccion();
            HashMap fieldsVende = new HashMap<>();
            fieldsVende.put("codigo", codigoVendedor);
            vendedor = vendeDao.getUniqueByFields(fieldsVende);
            vendeDao.commit();

            ventaDao.iniciarTransaccion();

            Venta venta = new Venta();
            venta.setId(ventaId);
<<<<<<< HEAD:core/src/main/java/cl/pizzaplaneta/ws/Pizzeria.java
                       venta.setFechaCreacion(new Date());
=======
            venta.setFechaCreacion(new Date());
>>>>>>> 85ee4e56706d5adabb7a1e0a55beccc28b8965ef:PizzaCore/src/main/java/cl/pizzaplaneta/ws/Pizzeria.java
            venta.setLocalId(local.getId());
            venta.setVendedorId(vendedor.getId());
            venta.setClienteId(c.getId());
            venta.setMontoTotal(montoTotal);
            ventaDao.insert(venta);
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date fechaEmision = venta.getFechaCreacion();
            String responseDate = df.format(fechaEmision);
            ventaDao.commit();

            VentaDetalleDao ventaDetalleDao = new VentaDetalleDao();
            
            for (VentaDetalle lstVenta : lstVentas) {
                ventaDetalleDao.iniciarTransaccion();
                ventaDetalleDao.insert(lstVenta);
                ventaDetalleDao.commit();
            }
            response.setVentaId(venta.getId());
            response.setMontoTotal(venta.getMontoTotal());

            response.setFecha(responseDate);
            response.setMessage("Ha sido exitosamente emitida la venta");
            response.setCode(200);

        } catch (Exception e) {
            ventaDao.rollback();
            response.setCode(201);
            response.setMessage("ERROR GENERAL, " + e.getMessage());
            e.printStackTrace();

        }

        return response;
    }

}
