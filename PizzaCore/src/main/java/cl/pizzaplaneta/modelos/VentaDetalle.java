/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.pizzaplaneta.modelos;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author ricardo
 */
@Entity
@Table(name = "ventas_detalles")
public class VentaDetalle extends Modelo implements Serializable {

    @Id
    @Column(name = "venta_detalle_id")
    String id;
    @Column(name = "venta_id")
    String ventaId;
    @Column(name = "producto_id")
    String productoId;
    @Column(name = "cantidad")
    Integer cantidad;
    @Column(name = "detalle_total")
    Long detalleTotal;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVentaId() {
        return ventaId;
    }

    public void setVentaId(String ventaId) {
        this.ventaId = ventaId;
    }

    public String getProductoId() {
        return productoId;
    }

    public void setProductoId(String productoId) {
        this.productoId = productoId;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Long getDetalleTotal() {
        return detalleTotal;
    }

    public void setDetalleTotal(Long detalleTotal) {
        this.detalleTotal = detalleTotal;
    }

}
