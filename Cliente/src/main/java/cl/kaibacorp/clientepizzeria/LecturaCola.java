/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.kaibacorp.clientepizzeria;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;
import cl.kaibacorp.clientepizzeria.ws.DetalleDto;
import cl.kaibacorp.clientepizzeria.ws.Pizzeria;
import cl.kaibacorp.clientepizzeria.ws.Pizzeria_Service;
import cl.kaibacorp.clientepizzeria.ws.Producto;
import cl.kaibacorp.clientepizzeria.ws.ProductoDto;
import cl.kaibacorp.clientepizzeria.ws.ProductoWs;
import cl.kaibacorp.clientepizzeria.ws.VentaDto;
import javax.jms.ObjectMessage;

/**
 *
 * @author ricardo
 */
public class LecturaCola implements Runnable { //Lectura es consumer

    private static boolean servicioActivo;
    private static Thread brokerThread;

    public static void iniciarServicio() {
        brokerThread = new Thread(new LecturaCola());
        brokerThread.setDaemon(true);
        brokerThread.start();
        servicioActivo = true;
    }

    public static void detenerServicio() {
        servicioActivo = false;
        brokerThread.interrupt();
    }

    private static void leer() {
        try {

            // Crea la conexion a la cola de mensajes
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("vm://localhost");
            Connection connection = connectionFactory.createConnection();
            connection.start();

            //connection.setExceptionListener(this);
            // Create una sesion en la cola
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Lee (o Crea si no existe) una cola con un "topico" o "asunto" 
            Destination destinationProducto = session.createQueue("producto-pizza-mq");
            Destination destinationVenta = session.createQueue("venta-pizza-mq");

            // Crea un "consumer" es decir un "lector" de la cola
            MessageConsumer consumerProducto = session.createConsumer(destinationProducto);
            MessageConsumer consumerVenta = session.createConsumer(destinationVenta);

            // Espera hasta recibir un mensaje
            while (servicioActivo) { //mientras el servicio este activo, voy a escuchar por un mensaje
                Message messageProducto = consumerProducto.receive(1000); //voy a esperar 1 segundo (1000 milisegundos) si es que llega un mensaje
                Message messageVenta = consumerVenta.receive(1000);
                if (null != messageProducto && messageProducto instanceof ObjectMessage) {
                    ObjectMessage objMessage = (ObjectMessage) messageProducto;
                    String text = objMessage.getStringProperty("tipo");
                    //AQUI LLAMADA WS CON VARIABLES ANTERIORMENTE DECLARADAS
                    System.out.println("tipo a: " + text);
                    objMessage.getObject();
                    System.out.println("Recibido a: " + text);
                } else {
                    System.out.println("Recibido b: " + messageProducto);
                }
                if (null != messageVenta && messageVenta instanceof ObjectMessage) {
                    ObjectMessage objMessage = (ObjectMessage) messageVenta;
                    String text = objMessage.getStringProperty("tipo");
                    //AQUI LLAMADA WS CON VARIABLES ANTERIORMENTE DECLARADAS
                    System.out.println("tipo c: " + text);
                    objMessage.getObject();
                    System.out.println("Recibido c: " + text);
                } else {
                   System.out.println("Recibido c: " + messageVenta);
                }
            }
            consumerProducto.close();
            consumerVenta.close();
            session.close();
            connection.close();
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void run() {
        leer();
    }

}
