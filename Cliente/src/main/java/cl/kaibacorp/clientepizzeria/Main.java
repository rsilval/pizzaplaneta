/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.kaibacorp.clientepizzeria;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.ws.BindingProvider;

/**
 *
 * @author Seto
 */
public class Main {
    
    public static final String TIPO_PRODUCTO = "producto";
    public static final String TIPO_VENTA = "venta";

    public static void main(String[] args) throws InterruptedException, MalformedURLException {
        Scanner sc = new Scanner(System.in);
        LecturaCola.iniciarServicio();

        while (true) {

            System.err.println("======================BIENVENIDO A PIZZA PLANETA======================");
            System.err.println(".---. .-..----..----. .--.   .---. .-.    .--. .-..-. .--. .-----. .--. ");
            System.err.println(": .; :: :`--. :`--. :: .; :  : .; :: :   : .; :: `: :: .--'`-. .-': .; :");
            System.err.println(":  _.': :  ,','  ,',':    :  :  _.': :   :    :: .` :: `;    : :  :    :");
            System.err.println(": :   : :.'.'_ .'.'_ : :: :  : :   : :__ : :: :: :. :: :__   : :  : :: :");
            System.err.println(":_;   :_;:____;:____;:_;:_;  :_;   :___.':_;:_;:_;:_;`.__.'  :_;  :_;:_;(TM)");

            System.out.println("Ingrese opcion:");
            System.out.println("1.- Registrar producto");
            System.out.println("2.- Registrar venta");
            System.out.println("Q.- Salir");
            System.out.print("Introduzca opcion: ");
            switch (sc.nextLine().toLowerCase()) {
                case "1":
                    registrarProducto();

                    break;
                case "2":
                    registrarVenta();
                    break;
                case "q":
                    System.out.println("=================CHAUU!!");
                    System.out.println(" .--. .---.  .--.  .--. .-. .--.  .--. .-.");
                    System.out.println(": .--': .; :: .; :: .--': :: .; :: .--': :");
                    System.out.println(": : _ :   .':    :: :   : ::    :`. `. : :");
                    System.out.println(": :; :: :.`.: :: :: :__ : :: :: : _`, ::_;");
                    System.out.println("`.__.':_;:_;:_;:_;`.__.':_;:_;:_;`.__.':_;");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Debe seleccionar una occion valida");
                    break;
            }

        }

    }

    public static void registrarProducto() {
        Scanner sc = new Scanner(System.in);
        String codigo = "";
        String nombre = "";
        String descripcion = "";
        Long precio = 0l;
        //En vez de consumir el WS, llamar la cola
        /*
        Producto p = new Producto();
        ProductoWs port = p.getProductoWsPort();
        BindingProvider binding = (BindingProvider) port;
        binding.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://localhost:8080/PizzaPlaneta-1.0/Producto");
         */
        System.out.print("Introduzca codigo: ");
        codigo = sc.nextLine();
        System.out.print("Introduzca nombre: ");
        nombre = sc.nextLine();
        System.out.print("Introduzca descripcion: ");
        descripcion = sc.nextLine();
        System.out.print("Introduzca precio: ");
        precio = sc.nextLong();
        /* En vez de consumir el Ws llamar la cola
        ProductoDto response = port.registrarProducto(nombre, descripcion, precio, codigo);
         */
//        System.out.println(response.getMessage());
//        System.out.println("Nombre: " + response.getNombre());
//        System.out.println("Descripcion: " + response.getDescripcion());
//        System.out.println("Precio: $" + response.getPrecioUnitario());
        MensajeProducto prodQ = new MensajeProducto();
        prodQ.setTipo(TIPO_PRODUCTO);
        prodQ.setCodigo(codigo);
        prodQ.setPrecio(precio);
        prodQ.setDescripcion(descripcion);
        prodQ.setNombre(nombre);
        
        EscribeCola.enviarMensaje(prodQ);
        
    }

    public static void registrarVenta() {

        Scanner sc = new Scanner(System.in);

        String fonoCliente = "";
        String codigoLocal = "";
        String codigoVendedor = "";
        String direccion = "";

//        Pizzeria_Service p = new Pizzeria_Service();
//        Pizzeria port = p.getPizzeriaPort();
//        BindingProvider binding = (BindingProvider) port;
//        binding.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://localhost:8080/PizzaPlaneta-1.0/Pizzeria");
        System.out.print("Introduzca codigo vendedor: ");
        codigoVendedor = sc.nextLine();
        System.out.print("Introduzca codigo local: ");
        codigoLocal = sc.nextLine();
        System.out.print("Introduzca fono cliente: ");
        fonoCliente = sc.nextLine();
        System.out.print("Introduzca dirección cliente: ");
        direccion = sc.nextLine();

        boolean ingresoProducto = true;
        List<DetalleDto> detalle = new ArrayList<>();

        while (ingresoProducto) {
            DetalleDto detail = new DetalleDto();
            System.out.print("Introduzca codigo producto: ");
            detail.setCodigoPizza(sc.nextLine());
            System.out.print("Introduzca cantidad: ");
            detail.setCantidad(sc.nextInt());
            detalle.add(detail);
            System.out.print("Necesitas ingresar nuevo producto (SI/NO)?");

            if (sc.nextLine().equalsIgnoreCase("SI")) {
                ingresoProducto = true;
            } else if (sc.nextLine().equalsIgnoreCase("NO")) {
                ingresoProducto = false;
            }

        }

//        VentaDto venta = port.realizarVenta(fonoCliente, direccion, detalle, codigoVendedor, codigoLocal);
//        System.err.println(venta.getMessage() + " el " + venta.getFecha() + " por un monto total de $" + venta.getMontoTotal());
    }

}
