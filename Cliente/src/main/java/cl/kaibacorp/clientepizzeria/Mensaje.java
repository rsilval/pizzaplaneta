/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cl.kaibacorp.clientepizzeria;

import java.io.Serializable;

/**
 *
 * @author ricardo
 */
abstract class Mensaje  implements Serializable {
    
    String tipo;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    

}
