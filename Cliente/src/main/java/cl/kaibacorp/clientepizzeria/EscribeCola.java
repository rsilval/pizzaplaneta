/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.kaibacorp.clientepizzeria;

import static cl.kaibacorp.clientepizzeria.Main.TIPO_PRODUCTO;
import static cl.kaibacorp.clientepizzeria.Main.TIPO_VENTA;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;
import cl.kaibacorp.clientepizzeria.ws.DetalleDto;
import cl.kaibacorp.clientepizzeria.ws.Pizzeria;
import cl.kaibacorp.clientepizzeria.ws.Pizzeria_Service;
import cl.kaibacorp.clientepizzeria.ws.Producto;
import cl.kaibacorp.clientepizzeria.ws.ProductoDto;
import cl.kaibacorp.clientepizzeria.ws.ProductoWs;
import cl.kaibacorp.clientepizzeria.ws.VentaDto;

/**
 *
 * @author ricardo
 */
public class EscribeCola {

    public static void enviarMensaje(Mensaje mensaje) {
        try {
            // Crea la conexion a la cola de mensajes
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("vm://localhost");
            connectionFactory.setTrustedPackages(new ArrayList(Arrays.asList("cl.kaibacorp.clientepizzeria")));
            Connection connection = connectionFactory.createConnection();
            connection.start();
            System.out.println("inicia conexion");
            // Crea una sesion de conexion
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = null;
            switch (mensaje.getTipo().toLowerCase()) {
                case TIPO_PRODUCTO:
                    System.out.println("mensaje producto");
                    destination = session.createQueue("producto-pizza-mq");
                    break;
                case TIPO_VENTA:
                    destination = session.createQueue("venta-pizza-mq");
                    break;
                default:
                    System.out.println("No es un tipo reconocible");
                    break;

            }
            // Create the destination (Topic or Queue)

            // Lee (o Crea si no existe) una cola con un "topico" o "asunto" 
            MessageProducer producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            // Crea un mensaje de tipo TEXTO
            ObjectMessage message = session.createObjectMessage(mensaje);
            System.out.println("producto encolado");
            // Muestra por la consola que encio el mensaje
            System.out.println("Mensaje enviado: " + message.hashCode());
            producer.send(message);

            // Cierra la sesion
            session.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
