/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.kaibacorp.clientepizzeria;

/**
 *
 * @author ricardo
 */
public class DetalleDto {

    private String codigoPizza;
    private int cantidad;

    public String getCodigoPizza() {
        return codigoPizza;
    }

    public void setCodigoPizza(String codigoPizza) {
        this.codigoPizza = codigoPizza;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }



}
